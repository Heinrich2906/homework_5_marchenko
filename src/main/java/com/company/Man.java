package com.company;

/**
 * Created by heinr on 15.10.2016.
 */
public class Man {

    private String name;
    private int weight;
    private int age;
    private Main.Sex sex;

    public void setAge(int age) {
        this.age = age;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setWeight(int weight) {this.weight = weight;}

    public String getName() {return name;}
    public int getWeight() {return weight;}
    public int getAge() {return age;}
}
