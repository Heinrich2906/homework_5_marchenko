package com.company;

import java.util.Random;

/**
 * Created by heinr on 15.10.2016.
 */
public class Matrix {

    int[][] matrix;
    int height;
    int width;

    Matrix(int weight, int height) {

        if (weight > 0 & height > 0) {

            Random rand = new Random();

            for (int i = 0; i < weight; i++) {
                for (int j = 0; j < height; j++) {
                    this.matrix[i][j] = rand.nextInt(1939);
                }
            }
        }
    }

    Matrix(int[][] matrix) {

        this.width = matrix.length;
        this.height = matrix[0].length;

        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                this.matrix[i][j] = matrix[i][j];
            }
        }
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public void setMatrix(int[][] matrix) {

        this.width = matrix.length;
        this.height = matrix[0].length;

        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                this.matrix[i][j] = matrix[i][j];
            }
        }
    }

    public int[][] addMatrix(int[][] anotherMatrix) {

        int[][] array;

        if ((this.width == anotherMatrix.length) & (this.height == anotherMatrix[0].length)) {

            array = new int[this.width][this.height];

            for (int i = 0; i < this.width; i++) {
                for (int j = 0; j < this.height; j++) {
                    array[i][j] = matrix[i][j] + anotherMatrix[i][j];
                }
            }
        } else {
            array = new int[0][0];
            System.out.print("Разный размер матриц");
        }
        return array;
    }

    public int[][] multiplyMatrix(int constanta) {

        int[][] array = new int[this.width][this.height];

        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                array[i][j] = matrix[i][j] + constanta;

            }
        }
        return array;
    }

    public int[][] multiplyMatrixByMatrix(int[][] anotherMatrix) {

        int[][] array;

        //Проверка согласованности матриц
        if (this.height == anotherMatrix.length) {

            array = new int[this.width][anotherMatrix[0].length];

            for (int i = 0; i < this.height; i++) {

                for (int j = 0; j < this.width; j++) {

                    array[i][j] = 0;

                    for (int k = 0; k < this.width; k++) {
                        array[i][j] = array[i][j] + this.matrix[i][k] * anotherMatrix[k][j];
                    }
                }
            }

        } else {
            array = new int[0][0];
            System.out.print("Матрицы не согласованы");
        }

        return array;

    }

    public int[][] transposition() {

        int[][] array = new int[this.height][this.width];

        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                array[j][i] = this.matrix[i][j];
            }
        }

        return array;
    }

    @Override
    public String toString() {

        String stroka = "";

        for (int i = 0; i < this.width; i++)
            for (int j = 0; j < this.height; j++)

                if (j == (this.height - 1)) {
                    //System.out.println(matrix[i][j]);
                    stroka = stroka + matrix[i][j] + "/";
                } else {
                    //System.out.print(matrix[i][j] + " ");
                    stroka = stroka + matrix[i][j] + " ";
                }

        return stroka;
    }

}


